<div align="center"><img src="app/src/main/res/mipmap-xxxhdpi/ic_launcher_round.png" width="192" /></div>

# <div align="center">Barcode Scanner</div>

<div align="center"><h4>A free app which allow to read and generate barcodes for Android.</h4></div>

[![License: GPLv3](https://img.shields.io/badge/License-GPLv3-blue)](https://www.gnu.org/licenses/gpl-3.0)

# Overview

Barcode scanner is a free and open source app which allow to read and generate barcodes. It can collect information about food products, cosmetics and books thanks to the databases Open Food Facts, Open Beauty Facts, Open Pet Food Facts and OpenLibrary.

# Download

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="90">](https://f-droid.org/packages/com.atharok.barcodescanner/)
[<img src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png"
     alt="Get it on Google Play"
     height="90">](https://play.google.com/store/apps/details?id=com.atharok.barcodescanner)
[<img src="https://images-na.ssl-images-amazon.com/images/G/01/mobile-apps/devportal2/res/images/amazon-appstore-badge-english-black.png"
     alt="Get it on Amazon Appstore"
     height="90">](https://www.amazon.com/Atharok-Barcode-Scanner/dp/B0BCDZ19T2)

# Screenshots

<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/01_qr_light.png" width="192" />
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/02_food_product_light.png" width="192" />
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/03_history_light.png" width="192" />
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/04_create_light.png" width="192" />
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/05_qr_dark.png" width="192" />
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/06_food_product_dark.png" width="192" />
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/07_history_dark.png" width="192" />
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/08_create_dark.png" width="192" />

# Donate

If you like Barcode Scanner you can donate via Liberapay.

[![Donate](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/Atharok/donate)

# Format

- 2D barcode format:
***QR Code, Data Matrix, PDF 417, AZTEC***

- 1D barcode format:
***EAN 13, EAN 8, UPC A, UPC E, Code 128, Code 93, Code 39, Codabar, ITF***

# Services

Collect information about a product during a scan:

- Food Products with [Open Food Facts](https://world.openfoodfacts.org/)
- Cosmetic Products with [Open Beauty Facts](https://world.openbeautyfacts.org/)
- Pet Food Products with [Open Pet Food Facts](https://world.openpetfoodfacts.org/)
- Books with [Open Library](https://openlibrary.org/)

# App features

- Simply point your smartphone's camera to a barcode and instantly receive information about it. You can also scan barcodes through a picture in your smartphone.
- With a simple scan, read business cards, add new contacts, add new events to your agenda, open URL or even connect to Wi-Fi.
- Scan Food products barcodes to receive information about their composition thanks to the databases Open Food Facts and Open Beauty Facts.
- Search information about the product you scan, with a quick research on different websites such as Amazon or Fnac.
- Keep track of all your scanned barcodes with the history tool.
- Generate your own barcodes
- Customize the interface with different colours, with a light theme or a dark one. The app integrates the Android 12's features, allowing to adjust colours depending on your wallpaper.
- Texts are entirely translated in English, Spanish, French, German and Russian.

# Licences

The code is licensed under the [GPLv3](https://www.gnu.org/licenses/gpl-3.0).

Dependencies:

- [ZXing](https://github.com/zxing/zxing) is licensed under [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0) by Zxing
- [Code Scanner](https://github.com/yuriy-budiyev/code-scanner) is licensed under [MIT License](https://mit-license.org/) by Yuriy Budiyev
- [ez-vcard](https://github.com/mangstadt/ez-vcard) is licensed under [FreeBSD](https://www.freebsd.org/copyright/freebsd-license/) by Michael Angstadt
- [Android Image Cropper](https://github.com/CanHub/Android-Image-Cropper) is licensed under [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0) by CanHub
- [Coil](https://github.com/coil-kt/coil) is licensed under [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0) by coil-kt
- [Material Components for Android](https://github.com/material-components/material-components-android) is licensed under [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0) by Material Components
- [Koin](https://github.com/InsertKoinIO/koin) is licensed under [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0) by insert-koin.io
- [Gson](https://github.com/google/gson) is licensed under [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0) by Google
- [Retrofit](https://github.com/square/retrofit) is licensed under [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0) by Square
- [Room](https://android.googlesource.com/platform/frameworks/support/+/refs/heads/androidx-main/room) is licensed under [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0) by Google

Images:

- [Material icons](https://fonts.google.com/icons) are licensed under [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0)
- [Images of the flags](https://www.drapeauxdespays.fr) are in the public domain
